## Version 2.0 Mar 2025
- **Change**: From version 2.0 Insplico will filter skipped junctions by default. Therefore, the way how Insplico is called has changed as the user now needs to specify the column with gene ids of the exon table by default. Thus, the exon table needs to contain a column with gene ids and it has to contain all known exons of all genes, including first and last exons of genes. 
- **Added**: Argument -notfilterskipped, which, when called, will cause Insplico not to filter skipped junctions, and therefore resemble the behavior of Insplico prior to version 2.0 for reproducibility.
- **Removed**: Argument -filterskipped, which as introduced in version 1.1.

## Version 1.1 Dec 2024
- **Extension:** Insplico got a new argument `-filterskipped`. With this argument Insplico will apply slight changes to the estimation of PSI values. In detail: While running Insplico without `-filterskipped` Insplico will consider all splice junctions that overspan an exon to be exclusion junctions and provide an exclusion count. This is the original approach. While it works fine, it may lead to a significant number of false exclusion counts in case where there are many rogue splice junctions, eg. overspanning the entire gene of the exon originating and terminating outside the gene. With argument `-filterskipped` Insplico will try to detect such rogue splice junctions and discard them from the PSI estimation. In this case the exon table must contain first and last exons of all genes so that Insplico knows about where genes start and end. Insplico's runtime decreases considerably when running with `-filterskipped`. It's important to note that in case of long reads, only potential rogue junctions will be discarded, not the entire read. Hence a long read with one rogue junction still may provide useful valid junctions that will be considered by Insplico.

## Version 1.0 Feb 2021
- First version
