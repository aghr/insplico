## Purpose: Analysis of alternative splicing for exons
Insplico is a software for estimating the splicing order of introns up- and downstream of exons from mapped RNA-seq reads (short and long, paired and single). For user-specified exons, Insplico extracts evidence for how often the upstream intron was spliced first, how often the downstream intron was spliced first, and percent spliced in (PSI) values.

## License: GNU GPLv3 
- Insplico is distributed under license GNU GPLv3
- Permissions: Commercial use, distribution, modification, patent use, private use
- Conditions: Disclose source, license and copyright notice, same license, state changes
- Limitations: Liability, warranty
- License text: file `COPYING`
- [See more info](https://choosealicense.com/licenses/gpl-3.0/)

## Citation
- See metadata: file `CITATION.cff`

## Documentation 
Can be found following the link to the Wiki in the left panel.