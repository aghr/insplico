#!/usr/bin/env perl


##############################################################################
#    extract_exons_from_gtf.pl is a software facilitating the use of Insplico
#    by allowing users to create exon tables from GTF-gene-descriptions to be
#    used with Insplico.
#
#    Copyright (C) 2024  Andre Gohr
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
##############################################################################




use Scalar::Util 'looks_like_number';

sub get_min_from_aref{
	$ret=9**9**9;
	foreach $v (@{$_[0]}){if($v<$ret){$ret=$v}}
return($ret);
}

sub get_max_from_aref{
	$ret=-1*9**9**9;
	foreach $v (@{$_[0]}){if($v>$ret){$ret=$v}}
return($ret);
}




$gtf=$ARGV[0];
$vts=$ARGV[1];
# '-chr' => choose elements from scaffolds with id staring with chr; '-num' => scaffold ids must start with a number
$scf_pattern='';
if($ARGV[1] && $ARGV[1] eq "-chr"){$scf_pattern="chr"; $vts=''}
if($ARGV[2] && $ARGV[2] eq "-chr"){$scf_pattern="chr";}
if($ARGV[1] && $ARGV[1] eq "-num"){$scf_pattern="num"; $vts=''}
if($ARGV[2] && $ARGV[2] eq "-num"){$scf_pattern="num";}


open($fh,$gtf) or die "$!";
while(<$fh>){@fs=split("\t");if(@fs<5){next}
	unless($fs[2] eq "exon"){next}
	if($scf_pattern){
		if($scf_pattern eq "chr" && $fs[0]!~/^[cC]hr/){next}
		if($scf_pattern eq "num" && $fs[0]!~/^\d/){next}
	}
	if(/gene_id \"(.+?)\"/){$gid=$1;
	if(/gene_name \"(.*?)\"/){$gn=lc($1); $gid{$gn}=$gid; $gn{$gid}=$gn}
	if(/gene_biotype \"(.+?)\"/){$gbt{$gid}=$1}
	if(/transcript_id \"(.+?)\"/){$tid=$1;
		push(@{$s{$gid}->{$tid}},$fs[3]);
		push(@{$e{$gid}->{$tid}},$fs[4]);
		$chr{$gid}=$fs[0];
		$str{$gid}=$fs[6];
	}}
}
close($fh);


%vts_c1s=();
%vts_c2s=();
if($vts){
	open($fh,$vts) or die "$!";
	while(<$fh>){@fs=split("\t");unless($fs[1]=~/EX/){next}; $gid=$gid{lc($fs[0])}; unless($gid){next}
		# chr1:100843177,100844743+100849022-100849196,100856288
		# chr3:171330069,171326091-171326156+171329424,171323206
		@fs2=split(/[:,-]/,$fs[4],-1);
		unless($fs2[0] eq $chr{$gid}){next}
		@c1s=split(/\+/,$fs2[1]); # when empty then likely first or last exon
		@c2s=split(/\+/,$fs2[4]); # when empty then likely first or last exon
		if($str{$gid} eq '-'){@c=@c1s;@c1s=@c2s;@c2s=@c}
		foreach $s (split(/\+/,$fs2[2])){foreach $e (split(/\+/,$fs2[3])){
			$vts_c1s{"$gid,$s,$e"}=[@c1s];
			$vts_c2s{"$gid,$s,$e"}=[@c2s];
			push(@{$s{$gid}->{"vasttools"}},$s);
			push(@{$e{$gid}->{"vasttools"}},$e);
		}}
	}
	close($fh);
}


print "C1\tSTARTS\tENDS\tC2\tCHR\tSTRAND\tEXONTYPE\tGID\tGNAME\tGBIOTYPE\tEXMINL\tEXMAXL\tMINDISTC1START\tMINDISTENDC2\tEXON_ID\tTOTALN_EXONS\n";
# EXONTYPE
# frst 
# sfrst
# intr
# slst
# lst
# vts
foreach $gid (keys %s){
	$chr=$chr{$gid};
	$str=$str{$gid};
	%c1=();
	%c2=();
	%exs=();
	%exs2=();
	%excs=();
	@chr=();
	@chr_s=();
	@chr_e=();
	$with_vts=0;
	%type=(); # exon type: second-first, internal, second-last

	foreach $tid (keys %{$s{$gid}}){
		@aa=@{$s{$gid}->{$tid}};
		@bb=@{$e{$gid}->{$tid}};
		@idx = sort { $aa[$a] <=> $aa[$b] } 0 .. $#aa;
		@aa = @aa[@idx];
		@bb = @bb[@idx];
		
		for($i=0;$i<@aa;$i++){
			$chr_s[$aa[$i]]=1;
			$chr_e[$bb[$i]]=1;
			$k="$aa[$i],$bb[$i]";
			$exs{$k}=1;
			unless($type{$k}){$type{$k}={}}
			unless($c1{$k}){$c1{$k}=[]}; unless($c2{$k}){$c2{$k}=[]}
			if($tid ne "vasttools"){
				if($i==0){
					$type{$k}->{"first"}=1;
					if(@aa>1){push(@{$c2{$k}},$aa[$i+1])}
					next;
				}
				if($i==@aa-1){
					$type{$k}->{"last"}=1;
					if(@aa>1){push(@{$c1{$k}},$bb[$i-1])}
					next;
				}
				if($i>1 && $i<@aa-2){$type{$k}->{"internal"}=1}
				if($i==1){$type{$k}->{"second-first"}=1}
				if($i==@aa-2){$type{$k}->{"second-last"}=1}
			
				push(@{$c1{$k}},$bb[$i-1]);
				push(@{$c2{$k}},$aa[$i+1]);
			}else{
				foreach $v2 (@{$vts_c1s{"$gid,$k"}}){$chr_e[$v2]=1} # we need to collect all C1 and C2 from VTS as exon ends and starts
				foreach $v2 (@{$vts_c2s{"$gid,$k"}}){$chr_s[$v2]=1} # because it's not guaranteed that all exon starts and ends in VTS are comprehensive 
				push(@{$c1{$k}},@{$vts_c1s{"$gid,$k"}});
				push(@{$c2{$k}},@{$vts_c2s{"$gid,$k"}});
				$type{$k}->{"vts"}=1;
				if(@{$vts_c1s{"$gid,$k"}}==0 && $str eq '+' || @{$vts_c2s{"$gid,$k"}}==0 && $str eq '-'){$type{$k}->{"first"}=1;}
				if(@{$vts_c1s{"$gid,$k"}}==0 && $str eq '-' || @{$vts_c2s{"$gid,$k"}}==0 && $str eq '+'){$type{$k}->{"last"}=1;}
			}
		}
	}
	# those exons that are not ir events
	@s=(); @e=();
        foreach $v (keys %exs){@fs=split(/,/,$v);push(@s,$fs[0]);push(@e,$fs[1])}
        for($i=0; $i<@s;$i++){
		$min=9**9**9;
		$max=-1;
                for($j=0;$j<@s;$j++){ 
			if($s[$j]<=$e[$i] && $e[$j]>=$s[$i]){
				if($s[$j]>$max){$max=$s[$j]}
				if($e[$j]<$min){$min=$e[$j]}
			}
                }
		if($max>$min){next} # maximal start > minimal end of all overlapping exons => IR
                $exs2{"$s[$i],$e[$i]"}=1;
        }

        $min=9**9**9;
        $max=-1;
	@s=(); @e=();
        foreach $v (keys %exs2){@fs=split(/,/,$v);
                if($fs[0]<$min){$min=$fs[0]}
                if($fs[1]>$max){$max=$fs[1]}
                for($i=$fs[0];$i<=$fs[1];$i++){$chr[$i]=1}
		push(@s,$fs[0]);
		push(@e,$fs[1]);
        }
        for($i=$min;$i<=$max;$i++){
                if(defined($chr[$i]) && !defined($chr[$i-1])){$s=$i}             # exon cluster starts here
                if(defined($chr[$i]) && !defined($chr[$i+1])){$excs{"$s,$i"}=1}  # exon cluster ends here
        }

	@lines=(); # will contain all exon clusters to be output; will be used to sort the output wrt exon cluster number
	@exon_starts=();
	%dont_output=();
	# get for all exon clusters all of its members
	foreach $v (keys %excs){@fs=split(/,/,$v);
		%s2=(); %e2=(); $c1=9**9**9; $c2=0; $min2=9**9**9; $max2=0; %types=();
		# which exons overlap with this cluster?
		for($i=0;$i<@s;$i++){
			unless($s[$i]<=$fs[1] && $e[$i]>=$fs[0]){next}  
			$k="$s[$i],$e[$i]";
			foreach $v2 (keys %{$type{$k}}){$types{$v2}=1}
			$min_tmp=get_min_from_aref($c1{$k});
			if($c1{$k} && $min_tmp < $c1){$c1=$min_tmp}
			$max_tmp=get_max_from_aref($c2{$k});
			if($c2{$k} && $max_tmp > $c2){$c2=$max_tmp}
			if($s[$i]<$min2){$min2=$s[$i]}
			if($e[$i]>$max2){$max2=$e[$i]}
			$s2{$s[$i]}=1; $e2{$e[$i]}=1;
		}

		@type_final=();
		if($str eq '+'){
			if($types{"first"}){push(@type_final,"frst")}
			if($types{"second-first"}){push(@type_final,"sfrst")}
			if($types{"internal"}){push(@type_final,"intr")}
			if($types{"second-last"}){push(@type_final,"slst")}
			if($types{"last"}){push(@type_final,"lst")}
			if($types{"vts"}){push(@type_final,"vts")}
		}else{
			if($types{"last"}){push(@type_final,"frst")}
			if($types{"second-last"}){push(@type_final,"sfrst")}
			if($types{"internal"}){push(@type_final,"intr")}
			if($types{"second-first"}){push(@type_final,"slst")}
			if($types{"first"}){push(@type_final,"lst")}
			if($types{"vts"}){push(@type_final,"vts")}
		}
		$type_final=join("_",@type_final); 

		if($c1==9**9**9){$c1=$min2}  # first exon
		if($c2==0){$c2=$max2}        # last exon

		@c1s=(); @c2s=();
		# collect all starts end ends until smallest c1 and largest c2
		for($i=$c1;$i<$min2;$i++){if($chr_e[$i]){push(@c1s,$i)}}
		for($i=$max2+1;$i<=$c2;$i++){if($chr_s[$i]){push(@c2s,$i)}}
		if(@c1s==0){$c1s[0]='NA'}
		if(@c2s==0){$c2s[0]='NA'}
	
		$minl=9**9**9;
		$maxl=-1;
		$mindc1=9**9**9;
		$mindc2=9**9**9;
		foreach $s (keys %s2){foreach $e (keys %e2){
			$l=$e-$s+1; 
			if($l<$minl){$minl=$l}; if($l>$maxl){$maxl=$l}
			if($s-$c1s[$#c1s]<$mindc1){$mindc1=$s-$c1s[$#c1s]-1}
			if($c2s[0]-$e<$mindc2){$mindc2=$c2s[0]-$e-1}
		}}
		
		$gn=$gn{$gid}; unless($gn){$gn="NA"}
		$gbt=$gbt{$gid}; unless($gbt){$gbt="NA"}
		$line="".join("+",@c1s)."\t".join("+",sort {$a<=>$b} keys %s2)."\t".join("+",sort {$a<=>$b} keys %e2)."\t".join("+",@c2s)."\t$chr\t$str\t$type_final\t$gid\t$gn\t$gbt\t$minl\t$maxl\t$mindc1\t$mindc2";
		push(@lines,$line);
		push(@exon_starts,get_min_from_aref(keys %s2));
		if($mindc1<20 || $mindc2<20){$dont_output{$line}=1}
	}

	@idx = sort { $exon_starts[$a] <=> $exon_starts[$b] } 0 .. $#exon_starts;
	@lines=@lines[@idx];
	for($i=0;$i<@lines;$i++){
		$line=$lines[$i];
		unless($don_output{$line}){
			if($str eq "+"){
				print "$line\t".($i+1)."\t".scalar(@lines)."\n";
			}else{
				print "$line\t".(@lines-$i)."\t".scalar(@lines)."\n";
			}
		}
	}
}
